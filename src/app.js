import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Payment from './components/paymentWindow';
import add from './pictures/Rectangle 29.png';
import Cards from './components/sponsors';
import About from './components/about';

function App() {
  return (
    <Container
      style={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
      }}
    >
      <div>
        <Row>
          <Col className="appRow">
            <header>
              <img src={add} alt="покупай" />
            </header>
          </Col>
        </Row>
        <Row>
          <Col className="appRow">
            <Cards />
          </Col>
        </Row>
        <Row>
          <Col className="mainRow">
            <Payment />
            <About />
          </Col>
        </Row>
      </div>
    </Container>
  );
}

export default App;
