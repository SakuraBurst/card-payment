/* eslint-disable consistent-return */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import React from 'react';
import { Form, Col } from 'react-bootstrap';

export default function DateInput(props) {
  const dateTest = /[^\d/]/;
  const yearInputRef = React.createRef();
  return (
    <Form.Group
      as={Col}
      style={{ display: 'flex', alignItems: 'center' }}
      md="8"
      xs="8"
      controlId="mounth"
    >
      <Form.Control
        placeholder="Mecяц"
        style={{ height: 62, marginRight: '5px' }}
        type="text"
        name="mounthInput"
        value={props.values.mounthInput}
        onBlur={props.handleBlur}
        onChange={e => {
          if (e.target.value.length === 2) {
            yearInputRef.current.focus();
          }
          if (dateTest.test(e.target.value)) {
            return;
          }
          return props.setFieldValue('mounthInput', e.target.value);
        }}
        isValid={props.touched.mounthInput && !props.errors.mounthInput}
        isInvalid={!!props.errors.mounthInput && props.touched.mounthInput}
      />
      <p
        style={
          props.color
            ? { textAlign: 'center', color: '#E1E5E7' }
            : { textAlign: 'center', color: '#9395A4' }
        }
      >
        /
      </p>
      <Form.Control
        placeholder="Год"
        style={{ height: 62, marginLeft: '5px' }}
        ref={yearInputRef}
        type="text"
        name="yearInput"
        value={props.values.yearInput}
        onBlur={props.handleBlur}
        onChange={e => {
          if (e.target.value.length > 2) {
            return;
          }
          if (dateTest.test(e.target.value)) {
            return;
          }
          return props.setFieldValue('yearInput', e.target.value);
        }}
        isValid={props.touched.yearInput && !props.errors.yearInput}
        isInvalid={!!props.errors.yearInput && props.touched.yearInput}
      />
    </Form.Group>
  );
}
