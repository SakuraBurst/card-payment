/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import React from 'react';
import { Form, Col } from 'react-bootstrap';

export default function UserEmail(props) {
  return (
    <Form.Group as={Col} xs="8" controlId="email">
      <Form.Control
        style={{ height: '62px' }}
        type="text"
        name="email"
        value={props.values.email}
        onBlur={props.handleBlur}
        onChange={props.handleChange}
        isValid={props.touched.email && !props.errors.email}
        isInvalid={!!props.errors.email && props.touched.email}
      />
      {props.touched.email && props.errors.email ? (
        <Form.Control.Feedback type="invalid" style={{ position: 'absolute' }}>
          {props.errors.email}
        </Form.Control.Feedback>
      ) : (
        <div />
      )}
    </Form.Group>
  );
}
