/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import mc from '../pictures/MSSecure.png';
import visa from '../pictures/VisaVerifed.png';
import dss from '../pictures/DSS.png';
import mir from '../pictures/MIrAcept.png';

export default function About() {
  return (
    <div
      style={{
        width: 234,
        fontSize: 12,
        marginLeft: '12px',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-end'
      }}
    >
      <div>
        <img src={visa} alt="visa verified" />
        <img src={mc} alt="masterCard verified" />
      </div>
      <div>
        <img src={dss} alt="pciDSS verified" />
        <img src={mir} alt="mir verified" />
      </div>
      <p className="grey">
        В системе Uniteller безопасность платежей и конфиденциальность введенной
        Вами информации обеспечивается использованием протокола TLS и другими
        специальными средствами. Ваша конфиденциальная информация хранится
        исключительно нами и ни при каких обстоятельствах не будет предоставлена
        третьим лицам за исключением случаев, предусмотренных законодательством.
      </p>
      <p className="grey">
        В случае возникновения вопросов Вы можете обратиться в Службу поддержки
        компании Uniteller по телефону{' '}
        <a className="aStyle" href="tel:8 800 707-67-19">
          {' '}
          8 800 707-67-19
        </a>{' '}
        или электронной почте
        <a className="aStyle" href="mailto: support@uniteller.ru">
          {' '}
          support@uniteller.ru
        </a>
      </p>
    </div>
  );
}
