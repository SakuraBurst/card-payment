/* eslint-disable consistent-return */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import React from 'react';
import { Form, Col } from 'react-bootstrap';

export default function Cvv(props) {
  const cvvTest = /[^\d/]/;
  return (
    <Form.Group as={Col} md="3" xs="3" controlId="cvv">
      <Form.Control
        style={{ height: 62 }}
        name="cvv"
        placeholder="CVV"
        value={props.values.cvv}
        onBlur={props.handleBlur}
        onChange={e => {
          if (e.target.value.length > 3) {
            return;
          }
          if (cvvTest.test(e.target.value)) {
            return;
          }
          return props.setFieldValue('cvv', e.target.value);
        }}
        isValid={props.touched.cvv && !props.errors.cvv}
        isInvalid={!!props.errors.cvv && props.touched.cvv}
        onKeyDown={evt => evt.key === 'e' && evt.preventDefault()}
      />
    </Form.Group>
  );
}
