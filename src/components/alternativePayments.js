import React from 'react';
import { Button } from 'react-bootstrap';
import gPay from '../pictures/Google_Pay_(GPay)_Logo-4.png';
import applePay from '../pictures/apPay.png';
import samsungPay from '../pictures/Samsung_Pay_Logo 1.png';

export default function AlternativePayments() {
  return (
    <div
      style={{
        marginTop: '15px',
        display: 'flex',
        justifyContent: 'space-between'
      }}
    >
      <Button className="altPay" variant="light">
        <img src={gPay} alt="gPay" />
      </Button>
      <Button
        className="altPay"
        style={{ backgroundColor: 'black' }}
        variant="dark"
      >
        <img src={applePay} alt="applePay" />
      </Button>
      <Button className="altPay" variant="primary">
        <img src={samsungPay} alt="samsungPay" />
      </Button>
    </div>
  );
}
