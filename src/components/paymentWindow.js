/* eslint-disable no-nested-ternary */
/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-script-url */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-alert */
/* eslint-disable consistent-return */
import React, { useState } from 'react';
import { Formik } from 'formik';
import { Button, Form, Spinner } from 'react-bootstrap';
import Cvv from './cvv';
import CardNumber from './cardNumber';
import DateInput from './date';
import UserEmail from './email';
import { schema } from '../yupSchema';
import PaymentInfo from './paymentInfo';
import AlternativePayments from './alternativePayments';

const Success = React.lazy(() => import('./success'));
const Failed = React.lazy(() => import('./failed'));

function Payment() {
  // eslint-disable-next-line no-unused-vars
  const [price, setPrice] = useState(404);
  const [cardColor, cardColorChange] = useState('#F3F5F7');
  const [backslashColor, backslashColorChange] = useState(false);
  const [loading, setLoading] = useState(false);
  const [success, setSucces] = useState(false);
  const [successDate, setSuccessDate] = useState('');
  const [successCard, setSuccesCard] = useState('');
  const [failed, setFailed] = useState(false);
  function retry() {
    setFailed(false);
  }
  function returnToStore() {
    setSucces(false);
  }
  return (
    <div
      style={{
        width: '590px',
        padding: '30px',
        border: '1px solid #E1E5E7',
        borderRadius: '6px'
      }}
    >
      {!success && !failed ? (
        <div>
          <h1>Оплата:{Number.isInteger(price) ? `${price},00` : price}₽</h1>
          <PaymentInfo />
          <div className="paymentWindow">
            <Formik
              initialValues={{
                email: '',
                cvv: '',
                cardNumber: '',
                mounthInput: '',
                yearInput: '',
                emailcheck: false
              }}
              validationSchema={schema}
              onSubmit={(values, { setSubmitting }) => {
                setLoading(true);
                const newDate = new Date();
                const mounth = values.mounthInput;
                const year = 20 + values.yearInput;
                setSuccessDate(
                  newDate
                    .toLocaleString('ru', {
                      year: 'numeric',
                      month: 'long',
                      day: 'numeric',
                      hour: 'numeric',
                      minute: 'numeric',
                      second: 'numeric'
                    })
                    .replace(/г\.,\s/, '')
                );
                setSuccesCard(
                  values.cardNumber
                    .replace(/\s/g, '')
                    .replace(/^([\d]{6})([\d]{6})([\d]{4})/, '$1******$3')
                );
                // eslint-disable-next-line eqeqeq
                if (year == newDate.getFullYear()) {
                  if (mounth < newDate.getMonth() + 1) {
                    setLoading(false);
                    setFailed(true);
                  }
                }
                if (mounth > 12 || year < newDate.getFullYear()) {
                  setLoading(false);
                  setFailed(true);
                } else {
                  setTimeout(() => {
                    setLoading(false);
                    setSubmitting(false);
                    setSucces(true);
                  }, 1400);
                }
              }}
            >
              {formik => (
                <Form onSubmit={formik.handleSubmit}>
                  <div
                    style={{
                      height: 230
                    }}
                  >
                    <div
                      style={{
                        position: 'absolute',
                        width: 386,
                        height: 228,
                        backgroundColor: cardColor,
                        border: '1px solid #E1E5E7',
                        borderRadius: 12,
                        zIndex: 1
                      }}
                    >
                      <Form.Row
                        style={{ display: 'flex', justifyContent: 'center' }}
                      >
                        <CardNumber
                          {...formik}
                          changeColor={cardColorChange}
                          changeBackSlashColor={backslashColorChange}
                        />
                      </Form.Row>
                      <Form.Row
                        style={{
                          marginTop: '30px',
                          display: 'flex',
                          marginLeft: '29px'
                        }}
                      >
                        <DateInput {...formik} color={backslashColor} />
                      </Form.Row>
                    </div>
                    <div
                      style={{
                        left: 175,
                        position: 'absolute',
                        width: 386,
                        height: 228,
                        backgroundColor: '#E7E9EC',
                        border: '1px solid #E1E5E7',
                        borderRadius: 12
                      }}
                    >
                      <div
                        style={{
                          width: '100%',
                          height: 62,
                          backgroundColor: '#CFD1D4',
                          marginTop: '20px'
                        }}
                      />
                      <Form.Row
                        style={{
                          marginRight: '29px',
                          marginTop: '46px',
                          display: 'flex',
                          justifyContent: 'flex-end'
                        }}
                      >
                        <Cvv {...formik} />
                      </Form.Row>
                    </div>
                  </div>
                  <Form.Row>
                    <Form.Group style={{ marginTop: '13px', display: 'flex' }}>
                      <Form.Check
                        name="emailcheck"
                        label="Запомнить карту"
                        onChange={formik.handleChange}
                        isInvalid={!!formik.errors.terms}
                        feedback={formik.errors.terms}
                        id="emailcheck"
                      />
                    </Form.Group>
                  </Form.Row>
                  <p className="solid">Отправить чек на Email</p>
                  <Form.Row>
                    <UserEmail {...formik} />
                    <Button
                      variant="success"
                      style={{ height: '62px', width: '150px' }}
                      type="submit"
                    >
                      {loading ? (
                        <Spinner animation="border" variant="light" />
                      ) : (
                        'Оплатить'
                      )}
                    </Button>
                  </Form.Row>
                </Form>
              )}
            </Formik>
          </div>
          <p style={{ marginTop: '15px' }} className="solid">
            Другие способы оплаты
          </p>
          <AlternativePayments />
        </div>
      ) : success ? (
        <React.Suspense fallback={<p>loading...</p>}>
          <Success
            cardNumber={successCard}
            price={price}
            dateTime={successDate}
            returnToStore={returnToStore}
          />
        </React.Suspense>
      ) : (
        <React.Suspense fallback={<p>loading...</p>}>
          <Failed retry={retry} />
        </React.Suspense>
      )}
    </div>
  );
}

export default Payment;
