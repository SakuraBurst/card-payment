/* eslint-disable consistent-return */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import React from 'react';
import { Form, Col } from 'react-bootstrap';

export default function CardNumber(props) {
  const cardTest = /[^\d\s]/;
  return (
    <Form.Group as={Col} md="10" xs="10" controlId="cardNumber">
      <Form.Control
        style={{ height: 62, marginTop: '20px' }}
        type="text"
        name="cardNumber"
        placeholder="Номер карты"
        value={props.values.cardNumber}
        onBlur={props.handleBlur}
        onChange={e => {
          if (e.target.value.length > 19) {
            return;
          }
          if (cardTest.test(e.target.value)) {
            return;
          }
          if (e.target.value.length < 4) {
            props.changeColor('#F3F5F7');
            props.changeBackSlashColor(false);
          }
          if (e.target.value.length >= 4) {
            const testedNumbers = e.target.value.slice(0, 4);
            switch (testedNumbers) {
              case '1111':
                props.changeColor('#2F9A41');
                props.changeBackSlashColor(true);
                break;
              case '2222':
                props.changeColor('#0B1C27');
                props.changeBackSlashColor(true);
                break;
              case '3333':
                props.changeColor('#EF3124');
                props.changeBackSlashColor(true);
                break;
              case '4444':
                props.changeColor('#00B2E1');
                props.changeBackSlashColor(true);
                break;
              default:
                props.changeColor('#F3F5F7');
                props.changeBackSlashColor(true);
                break;
            }
          }
          return props.setFieldValue(
            'cardNumber',
            e.target.value
              .replace(/\s?/g, '')
              .replace(/(\d{4})/g, '$1 ')
              .trim()
          );
        }}
        isValid={props.touched.cardNumber && !props.errors.cardNumber}
        isInvalid={!!props.errors.cardNumber && props.touched.cardNumber}
      />
    </Form.Group>
  );
}
