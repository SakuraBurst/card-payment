import React from 'react';
import mc from '../pictures/Mastercard-logo.svg';
import maestro from '../pictures/Maestro_2016.svg';
import visa from '../pictures/Visa_2014_logo_detail.svg';
import mir from '../pictures/Mir-logo.svg';
import unitaler from '../pictures/uniteller-logo.png';

export default function Cards() {
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'space-between',
        width: '590px'
      }}
    >
      <img src={unitaler} style={{ width: 93, height: 60 }} alt="Uniteller" />
      <div>
        <img src={mir} className="cardP" alt="MIR" />
        <img src={visa} className="cardP" alt="VISA" />
        <img src={maestro} className="cardP" alt="Maestro" />
        <img src={mc} className="cardP" alt="MasterCard" />
      </div>
    </div>
  );
}
