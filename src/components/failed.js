import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';
import failImg from '../pictures/Failed.png';

export default function Failed(props) {
  const { retry } = props;
  return (
    <div style={{ textAlign: 'center' }}>
      <img src={failImg} alt="failed" />
      <h1>Ошибка</h1>
      <p className="grey">
        Пожалуйста, проверьте правильность введенной информации по Вашей
        банковской карте и попробуйте еще раз.
      </p>
      <p className="grey">
        Просим Вас повторно ввести поля: Номер карты, Срок действия, Код CVC2
        или CVV2, Имя держателя карты. Мы не сохраняем их в целях Вашей
        безопасности.
      </p>
      <Button
        variant="success"
        onClick={retry}
        style={{ height: 62, width: 224 }}
      >
        Повторить
      </Button>
    </div>
  );
}
Failed.propTypes = {
  retry: PropTypes.func.isRequired
};
