/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';
import successImg from '../pictures/Sucess.png';

export default function Success(props) {
  const { dateTime, price, cardNumber, returnToStore } = props;
  return (
    <div style={{ textAlign: 'center' }}>
      <img src={successImg} alt="success" />
      <h1>Платёж успешно совершен</h1>
      <h4 className="grey">
        Подробная информация о Вашем платеже отправлена на электронный адрес,
        указанный во время платежа.
      </h4>
      <Button
        variant="success"
        onClick={returnToStore}
        style={{ marginTop: '15px' }}
      >
        Вернуться в магазин
      </Button>
      <hr style={{ width: '100%' }} />
      <div id="paymentInfo" style={{ textAlign: 'left' }}>
        <div className="infoBlock">
          <p className="grey">Дата и время платежа:</p>
          <div className="infoBlock-info">
            <p className="solid">{dateTime}</p>
          </div>
        </div>
        <div className="infoBlock">
          <p className="grey">Получатель платежа:</p>
          <div className="infoBlock-info">
            <p className="solid">
              МКУ «Организатор городского парковочного пространства»
              parkingkzn.ru
            </p>
          </div>
        </div>
        <div className="infoBlock">
          <p className="grey">Номер платежа:</p>
          <div className="infoBlock-info">
            <p className="solid">474426</p>
          </div>
        </div>
        <div className="infoBlock">
          <p className="grey">Сумма:</p>
          <div className="infoBlock-info">
            <p className="solid">
              {Number.isInteger(price) ? `${price},00` : price}₽
            </p>
          </div>
        </div>
        <div className="infoBlock">
          <p className="grey">Банковская карта:</p>
          <div className="infoBlock-info">
            <p className="solid">{cardNumber}</p>
          </div>
        </div>
        <div className="infoBlock">
          <p className="grey">Код авторизации:</p>
          <div className="infoBlock-info">
            <p className="solid">933559219614</p>
          </div>
        </div>
      </div>
      <p
        style={{
          cursor: 'pointer',
          color: '#2F9A41',
          textDecoration: 'underline',
          textDecorationStyle: 'solid',
          textDecorationColor: '#2F9A41'
        }}
      >
        Распечатать
      </p>
    </div>
  );
}
Success.propTypes = {
  dateTime: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  cardNumber: PropTypes.string.isRequired,
  returnToStore: PropTypes.func.isRequired
};
