import React from 'react';
import secureImg from '../pictures/locked 1.png';

export default function PaymentInfo() {
  return (
    <div
      id="paymentInfo"
      style={{
        width: '100%',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
      }}
    >
      <div id="info">
        <p className="solid">
          Магазин:
          <span className="grey"> Cоциальная карта</span>
        </p>
        <p className="solid">
          Номер Заказа:
          <span className="grey"> 6378209463</span>
        </p>
      </div>
      <div id="secure" style={{ display: 'flex', width: 141 }}>
        <img src={secureImg} alt="secure" style={{ height: 42, width: 62 }} />
        <p className="grey">Безопасная форма оплаты</p>
      </div>
    </div>
  );
}
