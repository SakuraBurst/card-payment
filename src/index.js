import 'regenerator-runtime/runtime';
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/style.css';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';

ReactDOM.render(<App />, document.getElementById('app'));
