import * as yup from 'yup';

// eslint-disable-next-line import/prefer-default-export
export const schema = yup.object({
  email: yup
    .string()
    .matches(/[\w]+@[\w]+\.[\w]+/, 'Введите правильный Email')
    .required('Укажите Email для отправки чека'),
  cvv: yup
    .string()
    .length(3, 'Must be 3')
    .required('CVV is Required'),
  cardNumber: yup
    .string()
    .matches(/^(\d{4}\s){3}\d{4}$/, 'Right card info pls')
    .max(19)
    .required('Card Number is Required'),
  mounthInput: yup
    .string()
    .matches(/^[\d]{2}/)
    .max(5)
    .required('Mounth is Required'),
  yearInput: yup
    .string()
    .matches(/^[\d]{2}/)
    .max(5)
    .required('Mounth is Required'),
  emailcheck: yup.bool()
});
